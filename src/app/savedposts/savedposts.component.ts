import { AuthService } from './../auth.service';
import { BlogPostsService } from './../blog-posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  like:number; 

  constructor(private blogpostService:BlogPostsService,public auth:AuthService) { }

  // addlike(id:string,likes:number){
  //   this.like+=1 ; 
  //   this.blogpostService.updatepostlike(this.userId,id,this.like)
  // }
  addlike(id:string,likes:number){
    this.like = likes + 1 ; 
    this.blogpostService.updatepostlike(this.userId,id,this.like)
  }

  deletpost(id){
    this.blogpostService.deletpost(this.userId,id);
   }
  

  ngOnInit() {
     
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
      this.posts$ = this.blogpostService.getpostofuser(this.userId);
         }
      )
  }

}
