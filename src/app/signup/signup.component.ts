import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit
{

  email:string;
  password:string

  constructor(public authservice:AuthService,
              public router:Router
              ) 
              { }

 createUser()
  {
    console.log('in CreateUser')
    this.authservice.signUp(this.email,this.password)
    console.log('after signUp in authService')
  } 

  ngOnInit() {
  }

}
