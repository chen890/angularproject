import { PostCommentsService } from './../post-comments.service';
import { Posts } from './../interfaces/posts';
import { BlogPostsService } from './../blog-posts.service';
import { BlogPost } from './../interfaces/blog-post';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Comm } from '../interfaces/comm';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comms$: Comm[];
  posts$: Posts[];
  postTitle;
  postId: number;
  post$: Observable<BlogPost>
  comments$:Observable<Comment>;


  constructor(private router: Router,
    private route: ActivatedRoute,
    private blogpostsService: BlogPostsService,
    private postcommentService: PostCommentsService, ) { }

  ngOnInit() {
    this.postId = this.route.snapshot.params.postId;

    this.post$ = this.blogpostsService.getSinglePost(this.postId);
    this.post$.subscribe(
      data => {
        console.log("data", data);
        this.postTitle = data.title;
     }
   )
    // console.log("title: ", this.postTitle)



    this.post$.subscribe(post => {
    this.postTitle = post.title
    })
    console.log("id: ", this.postId)
    this.comments$ = this.blogpostsService.getComments(this.postId);
  }

}
