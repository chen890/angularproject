import { AuthService } from './auth.service';
import { Comments } from './interfaces/comments';
import { BlogPost } from './interfaces/blog-post';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class BlogPostsService 
{
  userId;
  private POSTURL = "https://jsonplaceholder.typicode.com/posts";
  private SINGLEPOSTURL = "https://jsonplaceholder.typicode.com/posts/";
  
  apiUrlComm = 'https://jsonplaceholder.typicode.com/comments';
  private itemsCollection: AngularFirestoreCollection<BlogPost>;
  private COMMENTURL = "https://jsonplaceholder.typicode.com/comments?postId=";

  userCollection:AngularFirestoreCollection = this.db.collection('Users');
  postsCollection:AngularFirestoreCollection;


  constructor(private http: HttpClient, private db: AngularFirestore,
              private _http: HttpClient,
              public auth:AuthService) 
              {
                this.auth.user.subscribe(
                  user => {
                    this.userId = user.uid;
                   });
  }
  // updatepostlike(userId:string, id:string,like:number){
  //   console.log(userId)
  //   console.log(id)
  //   console.log(like)

  //   this.db.doc(`Users/${userId}/Posts/${id}`).update(
  //      {
  //        like:like
  //      }
  //    )
  //  }


  updatepostlike(userId:string, id:string,like:number){
    this.db.doc(`Users/${userId}/Posts/${id}`).update(
       {
         like:like
       }
     )
   }


   getpostofuser(userId):Observable<any[]>{
    this.postsCollection = this.db.collection(`Users/${userId}/Posts`);
        console.log('Posts collection created');
        return this.postsCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }
   deletpost(userId:string, id:string){
    this.db.doc(`Users/${userId}/Posts/${id}`).delete()
   }

    // addpost(userId:string,title:string, body:string)
    // {
      
    //   console.log(userId)
    //   console.log(title)
    //   console.log(body)
    //   const post = {title:title, body:body,like:0};
    //   this.userCollection.doc(userId).collection('Posts').add(post);
    // }

    saveToFirebase(id, uid, title, body){
      this.userCollection.doc(uid).collection('Posts').add({
        id: id,
        userId: uid,
        title: title,
        body: body
      })
    }

    getUserPosts(userId): Observable<any[]>
    {
      this.postsCollection = this.db.collection(`Users/${userId}/Posts`);
      return this.postsCollection.snapshotChanges().pipe(
                                                        map(
                                                            collection => collection.map(
                                                                                          document => 
                                                                                          {
                                                                                            const data = document.payload.doc.data();
                                                                                            data.id = document.payload.doc.id;
                                                                                            return data;
                                                                                          }))
                                                        );    
    } 

  getPosts(): Observable<BlogPost[]> {
    return this.http.get<BlogPost[]>(`${this.POSTURL}`);
  }


  getSinglePost(id: number): Observable<BlogPost> {
    return this.http.get<BlogPost>(`${this.SINGLEPOSTURL}${id}`);
  }
  
  getComments(postId:number): Observable<Comment>{
      console.log("URL: " ,`${this.COMMENTURL}${postId}`)
      return this.http.get<Comment>(`${this.COMMENTURL}${postId}`);
    }

    
  private transformWeatherData(data): BlogPost {
    return {
      id: data.id,
      userId: data.userId,
      title: data.title,
      body: data.body,
    }

  }

  getComm() 
  {
    return this._http.get<Comments[]>(this.apiUrlComm);  
  }

}
